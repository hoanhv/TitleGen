# Headline NLP Generator


## Vietnamese headline generator from cagegory: **Xe Cộ**


To run: 
-   Open **mainmodel.ipynb** (*require Jupyter notebook*)
-   Type a text (*words, sentence, ...*) in ```input = " "```

    *example: ```input = "Yamaha Exciter "```*
-   See output result.

## More information
Dataset: 1000 Article Crawled from Zingnews (Unavailable)

Environment: Conda - Python 3.6.9 (readable in *requirement.txt*)